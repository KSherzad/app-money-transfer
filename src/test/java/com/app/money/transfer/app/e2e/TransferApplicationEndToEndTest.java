package com.app.money.transfer.app.e2e;

import com.app.money.transfer.MoneyTransferApplication;
import com.app.money.transfer.accounts.models.AccountStatement;
import com.app.money.transfer.accounts.resources.AccountDto;
import com.app.money.transfer.transactions.resources.TransactionDto;
import com.app.money.transfer.transfers.resources.TransferInfo;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@Slf4j
public class TransferApplicationEndToEndTest {

    private static final String BASE_LOCAL_HOST_URL = "http://localhost";
    private final RestTemplate restTemplate = new RestTemplate();
    private final ObjectMapper mapper = new ObjectMapper();

    @Value("${local.server.port}")
    int port;

    @Test
    public void shouldTest_initialBalancesOfAccountsEE1AndEE2AndTransferAmount1000FromEE2AndValidateBalances() throws Exception {
        this.shouldCheck_initialBalanceOfAccountEE1_is1000AndItHasZeroTransactions();
        this.shouldCheck_initialBalanceOfAccountEE2_is2000AndItHasZeroTransactions();
//        this.shouldTransfer_amount1000FromEE2ToEE1Successfully();
//        this.shouldCheck_newBalanceOfAccountEE1_is2000AndItHasOneCreditedTransaction();
//        this.shouldCheck_newBalanceOfAccountEE2_is1000AndItHasOneDebitTransaction();
    }

    public void shouldCheck_initialBalanceOfAccountEE1_is1000AndItHasZeroTransactions() throws Exception {
        log.info("Should test balance of Account EE1 is 1000");
        HttpEntity<String> request = new HttpEntity<String>(this.getHeaders());
        String url = this.getUrl(String.format("accounts/%s/statement", "EE1"));

        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        assertTrue(response.getStatusCode().is2xxSuccessful());

        DocumentContext context = JsonPath.parse(response.getBody());
        AccountDto accountEE1 = mapper.convertValue(context.read("$['account']"), AccountDto.class);

        assertNotNull(accountEE1);
        assertEquals("EE1", accountEE1.getAccountNumber());
        assertEquals(new BigDecimal("1000.0"), accountEE1.getBalance());

        List<TransactionDto> transactions =
                mapper.convertValue(context.read("$[*]['content'][0]"), new TypeReference<List<Object>>() { });

        assertTrue(transactions.isEmpty());
    }

    public void shouldCheck_initialBalanceOfAccountEE2_is2000AndItHasZeroTransactions() throws Exception {
        log.info("Should test balance of Account EE2 is 2000");
        HttpEntity<String> request = new HttpEntity<String>(this.getHeaders());
        String url = this.getUrl(String.format("accounts/%s/statement", "EE2"));

        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        assertTrue(response.getStatusCode().is2xxSuccessful());

        DocumentContext context = JsonPath.parse(response.getBody());
        AccountDto accountEE2 = mapper.convertValue(context.read("$['account']"), AccountDto.class);

        assertNotNull(accountEE2);
        assertEquals("EE2", accountEE2.getAccountNumber());
        assertEquals(new BigDecimal("2000.0"), accountEE2.getBalance());

        List<TransactionDto> transactions =
                new ObjectMapper().convertValue(context.read("$[*]['content'][0]"), new TypeReference<List<TransactionDto>>() { });

        assertTrue(transactions.isEmpty());

        /*ResponseEntity<AccountStatement> response = restTemplate.getForEntity(url, AccountStatement.class);

        assertTrue(response.getStatusCode().is2xxSuccessful());
        AccountStatement accountStatement = response.getBody();
        AccountDto accountEE2 = accountStatement.getAccount();
        assertNotNull(accountEE2);
        assertEquals("EE2", accountEE2.getAccountNumber());
        assertEquals(new BigDecimal("2000.0"), accountEE2.getBalance());
        assertTrue(accountStatement.getTransactions().isEmpty());*/
    }

    public void shouldTransfer_amount1000FromEE2ToEE1Successfully() throws Exception {
        log.info("Should test transfer of Amount 1000 from Account EE2 to Account EE100");

        String url = this.getUrl("transfers");

        TransferInfo transferInfo = TransferInfo.builder().sourceAccountNumber("EE2")
                .destinationAccountNumber("EE1").amount(new BigDecimal(1000)).build();

        String requestBody = mapper.writeValueAsString(transferInfo);

        HttpEntity<String> request = new HttpEntity<String>(requestBody, this.getHeaders());

        ResponseEntity<AccountDto> response = restTemplate.postForEntity(url, request, AccountDto.class);

        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getId());
    }

    public void shouldCheck_newBalanceOfAccountEE1_is2000AndItHasOneCreditedTransaction() throws Exception {
        log.info("Should test balance of Account EE1 is 2000");

        HttpEntity<String> request = new HttpEntity<String>(this.getHeaders());
        String url = this.getUrl(String.format("accounts/%s/statement", "EE1"));
        ResponseEntity<AccountStatement> response = restTemplate.getForEntity(url, AccountStatement.class);

        assertTrue(response.getStatusCode().is2xxSuccessful());
        AccountStatement accountStatement = response.getBody();
        AccountDto accountEE1 = accountStatement.getAccount();
        assertNotNull(accountEE1);
        assertEquals("EE1", accountEE1.getAccountNumber());
        assertEquals(new BigDecimal("2000.0"), accountEE1.getBalance());
//        Page<TransactionDto> transactions = accountStatement.getTransactions();
//        assertFalse(transactions.isEmpty());
//        assertEquals(1, transactions.size());
//        assertEquals(new BigDecimal(1000), transactions.get(0).getCredit());
    }

    public void shouldCheck_newBalanceOfAccountEE2_is1000AndItHasOneDebitTransaction() throws Exception {
        log.info("Should test balance of Account EE2 is 1000");

        HttpEntity<String> request = new HttpEntity<String>(this.getHeaders());
        String url = this.getUrl(String.format("accounts/%s/statement", "EE2"));
        ResponseEntity<AccountStatement> response = restTemplate.getForEntity(url, AccountStatement.class);

        assertTrue(response.getStatusCode().is2xxSuccessful());
        AccountStatement accountStatement = response.getBody();
        AccountDto accountEE2 = accountStatement.getAccount();
        assertNotNull(accountEE2);
        assertEquals("EE2", accountEE2.getAccountNumber());
        assertEquals(new BigDecimal("1000.0"), accountEE2.getBalance());
//        List<TransactionDto> transactionsEE2 = accountStatement.getTransactions();
//        assertFalse(transactionsEE2.isEmpty());
//        assertEquals(1, transactionsEE2.size());
//        assertEquals(new BigDecimal(-1000), transactionsEE2.get(0).getDebit());
    }

    private String getUrl(String resourceAddress) {
        return String.format("%s:%s/%s", BASE_LOCAL_HOST_URL, port, resourceAddress);
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return headers;
    }
}
