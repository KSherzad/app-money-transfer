package com.app.money.transfer;

import com.app.money.transfer.accounts.models.AccountBo;
import com.app.money.transfer.accounts.resources.AccountDto;
import com.app.money.transfer.clients.models.ClientBo;
import com.app.money.transfer.transactions.models.TransactionBo;
import com.app.money.transfer.transactions.resources.TransactionDto;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

public class TestHelper {
    public static List<AccountDto> getAccountList() {
        return Collections.singletonList(getAccountEE1000());
    }

    public static final String ACCOUNT_NUMBER_EE1000 = "EE1000";
    public static final String ACCOUNT_NUMBER_EE2000 = "EE2000";
    public static final BigDecimal ACCOUNT_BALANCE_1000 = new BigDecimal("1000.0");
    public static final BigDecimal ACCOUNT_BALANCE_2000 = new BigDecimal("2000.0");
    public static final BigDecimal TRANSFER_AMOUNT_100 = new BigDecimal("100.0");
    public static final BigDecimal CREDIT_AMOUNT = new BigDecimal("100.0");

    public static AccountDto getAccountEE1000() {
        return AccountDto.builder()
                .accountNumber(ACCOUNT_NUMBER_EE1000)
                .balance(ACCOUNT_BALANCE_1000)
                .build();
    }

    public static AccountDto getAccountEE2000() {
        return AccountDto.builder()
                .accountNumber(ACCOUNT_NUMBER_EE2000)
                .balance(ACCOUNT_BALANCE_2000)
                .build();
    }

    public static AccountBo getAccountEE1000Bo() {
        return AccountBo.builder()
                .accountNumber(ACCOUNT_NUMBER_EE1000)
                .balance(ACCOUNT_BALANCE_1000)
                .build();
    }

    public static AccountBo getAccountEE2000Bo() {
        return AccountBo.builder()
                .accountNumber(ACCOUNT_NUMBER_EE2000)
                .balance(ACCOUNT_BALANCE_2000)
                .build();
    }

    /*public static TransferHistoryBo getTransferHistoryBo() {
        return TransferHistoryBo.builder()
                .amount(TRANSFER_AMOUNT_100)
                .sourceAccount(getAccountEE2000Bo())
                .destinationAccount(getAccountEE2000Bo())
                .build();
    }

    public static TransferDto getTransferHistory() {
        return TransferDto.builder()
                .amount(TRANSFER_AMOUNT_100)
                .sourceAccount(getAccountEE1000())
                .destinationAccount(getAccountEE2000())
                .build();
    }*/

    public static TransactionDto getTransaction() {
        return null; //TransactionDto.builder().credit(CREDIT_AMOUNT).build();
    }

    public static TransactionBo getTransactionBo() {
        return null; // TransactionBo.builder().credit(CREDIT_AMOUNT).build();
    }

    public static ClientBo getClientBo() {
        return ClientBo.builder().firstName("FName").lastName("LName")
                .address("Address").contact("Contact").build();
    }
}
