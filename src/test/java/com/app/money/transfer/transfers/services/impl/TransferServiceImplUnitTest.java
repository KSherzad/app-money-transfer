package com.app.money.transfer.transfers.services.impl;

import com.app.money.transfer.TestHelper;
import com.app.money.transfer.accounts.services.AccountService;
import com.app.money.transfer.accounts.services.impl.AccountServiceImpl;
import com.app.money.transfer.exceptions.TransferInvalidException;
import com.app.money.transfer.transactions.resources.TransactionDto;
import com.app.money.transfer.transactions.services.TransactionService;
import com.app.money.transfer.transactions.services.impl.TransactionServiceImpl;
import com.app.money.transfer.transfers.resources.TransferInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceImplUnitTest {

    private final AccountService accountService = mock(AccountServiceImpl.class);

    private final TransactionService transactionService = mock(TransactionServiceImpl.class);

    private final TransferServiceImpl transferService = new TransferServiceImpl(accountService, transactionService);

    @Test(expected = TransferInvalidException.class)
    public void executeTransfer_shouldThrowInvalidTransfer_whenSameAccountsForTransferIsProvided() {
        //given
        TransferInfo transferInfo = TransferInfo.builder()
                .amount(TestHelper.TRANSFER_AMOUNT_100)
                .destinationAccountNumber(TestHelper.ACCOUNT_NUMBER_EE1000)
                .sourceAccountNumber(TestHelper.ACCOUNT_NUMBER_EE1000).build();

        // when // then
        transferService.executeTransfer(transferInfo);
    }

    @Test
    public void executeTransfer_shouldPerformTransfer_whenValidTransferInfoIsProvided() {
        //given
        TransactionDto transactionDto = TransactionDto.builder()
                .amount(TestHelper.TRANSFER_AMOUNT_100)
                .destinationAccount(TestHelper.getAccountEE1000())
                .sourceAccount(TestHelper.getAccountEE2000()).build();
        when(transactionService.save(notNull())).thenReturn(transactionDto);

        TransferInfo transferInfo = TransferInfo.builder()
                .amount(TestHelper.TRANSFER_AMOUNT_100)
                .destinationAccountNumber(TestHelper.ACCOUNT_NUMBER_EE1000)
                .sourceAccountNumber(TestHelper.ACCOUNT_NUMBER_EE2000).build();

        // when
        TransactionDto savedTransaction = transferService.executeTransfer(transferInfo);

        // then
        assertNotNull(savedTransaction);
        assertNotNull(savedTransaction.getDestinationAccount());
        assertNotNull(savedTransaction.getSourceAccount());
        assertEquals(TestHelper.CREDIT_AMOUNT, savedTransaction.getAmount());
    }
}
