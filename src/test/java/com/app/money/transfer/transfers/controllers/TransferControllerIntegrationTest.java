package com.app.money.transfer.transfers.controllers;

import com.app.money.transfer.AbstractControllerIntegrationTest;
import com.app.money.transfer.TestHelper;
import com.app.money.transfer.accounts.models.AccountBo;
import com.app.money.transfer.accounts.repositories.AccountRepository;
import com.app.money.transfer.clients.models.ClientBo;
import com.app.money.transfer.clients.repositories.ClientRepository;
import com.app.money.transfer.exceptions.resources.ErrorType;
import com.app.money.transfer.transfers.resources.TransferInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class TransferControllerIntegrationTest extends AbstractControllerIntegrationTest {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AccountRepository accountRepository;

    private ClientBo clientBo;
    private ObjectMapper mapper;

    @Before
    public void setUp() {
        super.setUp();
        mapper = new ObjectMapper();
        this.initializeTestDataWithTwoAccounts();
    }

    @Test
    public void executeTransfer_shouldReturnBadRequest_whenNoRequestBodyIsProvided() throws Exception {

        mockMvc.perform(post("/transfers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorType", is(ErrorType.DATA_INVALID_EXCEPTION.name())));
    }

    @Test
    public void executeTransfer_shouldReturnValidationException_whenEmptyTransferInfoISProvided() throws Exception {

        mockMvc.perform(post("/transfers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(new TransferInfo())))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorType", is(ErrorType.DATA_INVALID_EXCEPTION.name())));
    }

    @Test
    public void executeTransfer_shouldReturnValidationException_whenAnEmptyRequiredFieldISProvided() throws Exception {

        TransferInfo transferInfo = TransferInfo.builder()
                .amount(TestHelper.TRANSFER_AMOUNT_100)
                .sourceAccountNumber(TestHelper.ACCOUNT_NUMBER_EE1000)
                .build();
        mockMvc.perform(post("/transfers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(transferInfo)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorType", is(ErrorType.DATA_INVALID_EXCEPTION.name())));
    }

    @Test
    public void executeTransfer_shouldReturnValidationException_whenZeroAmountIsProvided() throws Exception {

        TransferInfo transferInfo = TransferInfo.builder()
                .amount(BigDecimal.ZERO)
                .destinationAccountNumber(TestHelper.ACCOUNT_NUMBER_EE1000)
                .sourceAccountNumber(TestHelper.ACCOUNT_NUMBER_EE2000)
                .build();

        mockMvc.perform(post("/transfers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(transferInfo)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorType", is(ErrorType.DATA_INVALID_EXCEPTION.name())));
    }

    @Test
    public void executeTransfer_shouldReturnValidationException_whenNegativeAmountIsProvided() throws Exception {

        TransferInfo transferInfo = TransferInfo.builder()
                .amount(new BigDecimal(-100))
                .sourceAccountNumber(TestHelper.ACCOUNT_NUMBER_EE1000)
                .destinationAccountNumber(TestHelper.ACCOUNT_NUMBER_EE2000)
                .build();

        mockMvc.perform(post("/transfers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(transferInfo)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorType", is(ErrorType.DATA_INVALID_EXCEPTION.name())));
    }
    @Test
    public void executeTransfer_shouldReturnInSufficientBalanceException_whenAmountMoreThanAvailableIsProvided() throws Exception {

        TransferInfo transferInfo = TransferInfo.builder()
                .amount(new BigDecimal(2100))
                .destinationAccountNumber(TestHelper.ACCOUNT_NUMBER_EE1000)
                .sourceAccountNumber(TestHelper.ACCOUNT_NUMBER_EE2000)
                .build();

        mockMvc.perform(post("/transfers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(transferInfo)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorType", is(ErrorType.BALANCE_INSUFFICIENT_EXCEPTION.name())));
    }

    @Test
    public void executeTransfer_shouldReturnInvalidTransferException_whenTransferForSameAccountIsProvided() throws Exception {

        TransferInfo transferInfo = TransferInfo.builder()
                .amount(TestHelper.TRANSFER_AMOUNT_100)
                .sourceAccountNumber(TestHelper.ACCOUNT_NUMBER_EE1000)
                .destinationAccountNumber(TestHelper.ACCOUNT_NUMBER_EE1000)
                .build();

        mockMvc.perform(post("/transfers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(transferInfo)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorType", is(ErrorType.TRANSFER_INVALID_EXCEPTION.name())));
    }

    @Test
    public void executeTransfer_shouldTransferAmount_whenValidBankAccountsAndAmountAreProvided() throws Exception {

        TransferInfo transferInfo = TransferInfo.builder()
                .amount(TestHelper.TRANSFER_AMOUNT_100)
                .sourceAccountNumber(TestHelper.ACCOUNT_NUMBER_EE2000)
                .destinationAccountNumber(TestHelper.ACCOUNT_NUMBER_EE1000)
                .build();

        mockMvc.perform(post("/transfers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(transferInfo)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", notNullValue()))
                .andExpect(jsonPath("$.amount", is(100.0)))
                .andExpect(jsonPath("$.sourceAccount.accountNumber", is(TestHelper.ACCOUNT_NUMBER_EE2000)))
                .andExpect(jsonPath("$.sourceAccount.balance", is(1900.0)))
                .andExpect(jsonPath("$.destinationAccount.accountNumber", is(TestHelper.ACCOUNT_NUMBER_EE1000)))
                .andExpect(jsonPath("$.destinationAccount.balance", is(1100.0)));
    }

    private void initializeTestDataWithTwoAccounts() {

        clientBo = clientRepository.save(TestHelper.getClientBo());

        AccountBo accountEE1000Bo = TestHelper.getAccountEE1000Bo();
        accountEE1000Bo.setClient(clientBo);
        accountRepository.save(accountEE1000Bo);

        AccountBo accountEE2000Bo = TestHelper.getAccountEE2000Bo();
        accountEE2000Bo.setClient(clientBo);
        accountRepository.save(accountEE2000Bo);
    }
}
