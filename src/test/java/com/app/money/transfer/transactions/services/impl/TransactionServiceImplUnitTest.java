package com.app.money.transfer.transactions.services.impl;

import com.app.money.transfer.TestHelper;
import com.app.money.transfer.accounts.resources.AccountDto;
import com.app.money.transfer.transactions.models.TransactionBo;
import com.app.money.transfer.transactions.repositories.TransactionRepository;
import com.app.money.transfer.transactions.resources.TransactionDto;
import org.apache.commons.collections.CollectionUtils;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Collections;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceImplUnitTest {

    private final TransactionRepository transactionRepository = mock(TransactionRepository.class);
    private final Mapper mapper = new DozerBeanMapper();
    private final TransactionServiceImpl transactionService = new TransactionServiceImpl(transactionRepository, mapper);

    private TransactionBo transactionBo;
    private TransactionDto transaction;

    @Before
    public void setUp() {
        transaction = TransactionDto.builder().amount(TestHelper.CREDIT_AMOUNT).build();
        transactionBo = TransactionBo.builder().amount(TestHelper.CREDIT_AMOUNT).build();
    }

    @Test
    public void save_shouldSave_whenATransactionIsProvided() {
        // given
        when(transactionRepository.save(transactionBo)).thenReturn(transactionBo);

        //when
        TransactionDto savedTransaction = transactionService.save(transaction);

        //then
        assertNotNull(savedTransaction);
        assertEquals(TestHelper.CREDIT_AMOUNT, savedTransaction.getAmount());
    }

    @Test
    public void findTransactionByAccount_shouldReturnTransactions_whenAValidAccountIsProvided() {
        // given
        AccountDto account = TestHelper.getAccountEE1000();
        when(transactionRepository.findAllBySourceAccountOrDestinationAccount(notNull(), notNull(), notNull()))
                .thenReturn(new PageImpl<>(Collections.singletonList(transactionBo), Pageable.unpaged(), 1));

        // when
        Page<TransactionDto> transactions = transactionService.findTransactionsByAccount(account, Pageable.unpaged());

        // then
        assertTrue(CollectionUtils.isNotEmpty(transactions.getContent()));
        assertEquals(1, transactions.getContent().size());
        assertEquals(TestHelper.CREDIT_AMOUNT, transactions.getContent().get(0).getAmount());
    }
}
