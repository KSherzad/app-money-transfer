package com.app.money.transfer.transactions.repositories;

import com.app.money.transfer.TestHelper;
import com.app.money.transfer.accounts.models.AccountBo;
import com.app.money.transfer.accounts.repositories.AccountRepository;
import com.app.money.transfer.clients.models.ClientBo;
import com.app.money.transfer.clients.repositories.ClientRepository;
import com.app.money.transfer.transactions.models.TransactionBo;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@EnableJpaAuditing
@DataJpaTest
public class TransactionRepositoryIntegrationTest {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ClientRepository clientRepository;

    private AccountBo accountEE1000Bo;
    private AccountBo accountEE2000Bo;

    @Before
    public void setUp() {
        this.initializeDataWithTwoAccounts();
    }

    @Test
    public void findAllBySourceAccountOrDestinationAccount_shouldReturnEmptyTransactions_whenNoTransactionForAccountExists() {
        // given // when
        Page<TransactionBo> transactions =
                transactionRepository.findAllBySourceAccountOrDestinationAccount(accountEE1000Bo, accountEE1000Bo, Pageable.unpaged());

        // then
        assertTrue(transactions.getContent().isEmpty());
    }

    @Test
    public void findAllBySourceAccountOrDestinationAccount_shouldReturnAllTransactions_whenValidAccountIsProvided() {
        // given
        TransactionBo transactionBo = TransactionBo.builder()
                .amount(TestHelper.TRANSFER_AMOUNT_100)
                .sourceAccount(accountEE2000Bo)
                .destinationAccount(accountEE1000Bo).build();
        transactionBo = transactionRepository.save(transactionBo);

        // when
        Page<TransactionBo> transactions =
                transactionRepository.findAllBySourceAccountOrDestinationAccount(accountEE1000Bo, accountEE1000Bo, Pageable.unpaged());

        // then
        assertFalse(transactions.isEmpty());
        assertEquals(1, transactions.getContent().size());
        assertEquals(transactionBo, transactions.getContent().get(0));
    }

    private void initializeDataWithTwoAccounts() {
        ClientBo clientBo = TestHelper.getClientBo();
        clientRepository.save(clientBo);

        accountEE1000Bo = TestHelper.getAccountEE1000Bo();
        accountEE1000Bo.setClient(clientBo);
        accountEE1000Bo = accountRepository.save(accountEE1000Bo);

        accountEE2000Bo = TestHelper.getAccountEE2000Bo();
        accountEE2000Bo.setClient(clientBo);
        accountEE2000Bo = accountRepository.save(accountEE2000Bo);
    }
}
