package com.app.money.transfer.accounts.repositories;

import com.app.money.transfer.TestHelper;
import com.app.money.transfer.accounts.models.AccountBo;
import com.app.money.transfer.clients.models.ClientBo;
import com.app.money.transfer.clients.repositories.ClientRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.LockModeType;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryIntegrationTest {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Test
    public void findByAccountNumber_shouldReturnEmptyOptional_whenBlankAccountNumberIsProvided() {
        // given // when
        Optional<AccountBo> savedAccountOptional = accountRepository.findByAccountNumber(null, LockModeType.PESSIMISTIC_WRITE);

        // then
        assertFalse(savedAccountOptional.isPresent());
    }

    @Test
    public void findByAccountNumber_shouldReturnEmptyOptional_whenAccountDoesNotExist() {
        // given // when
        Optional<AccountBo> savedAccountOptional = accountRepository.findByAccountNumber("someAccount", LockModeType.PESSIMISTIC_WRITE);

        // then
        assertFalse(savedAccountOptional.isPresent());
    }

    @Test
    public void findByAccountNumber_shouldReturnOptionalOfAccount_whenAccountExists() {
        // given
        ClientBo clientBo = TestHelper.getClientBo();
        clientRepository.save(clientBo);
        AccountBo accountBo = TestHelper.getAccountEE1000Bo();
        accountBo.setClient(clientBo);
        accountRepository.save(accountBo);

        // when
        Optional<AccountBo> savedAccountOptional =
                accountRepository.findByAccountNumber(accountBo.getAccountNumber(), LockModeType.PESSIMISTIC_WRITE);

        // then
        assertTrue(savedAccountOptional.isPresent());
        assertEquals(accountBo, savedAccountOptional.get());
    }
}
