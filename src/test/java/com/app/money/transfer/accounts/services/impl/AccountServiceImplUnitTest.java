package com.app.money.transfer.accounts.services.impl;

import com.app.money.transfer.TestHelper;
import com.app.money.transfer.accounts.models.AccountBo;
import com.app.money.transfer.accounts.models.AccountStatement;
import com.app.money.transfer.accounts.repositories.AccountRepository;
import com.app.money.transfer.accounts.resources.AccountDto;
import com.app.money.transfer.exceptions.AccountNotFoundException;
import com.app.money.transfer.exceptions.BalanceInsufficientException;
import com.app.money.transfer.transactions.resources.TransactionDto;
import com.app.money.transfer.transactions.services.TransactionService;
import com.app.money.transfer.transactions.services.impl.TransactionServiceImpl;
import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplUnitTest {

    private final TransactionService transactionService = mock(TransactionServiceImpl.class);

    private final AccountRepository accountRepository = spy(AccountRepository.class);

    private final Mapper mapper = new DozerBeanMapper();

    @Spy
    private final AccountServiceImpl accountService = new AccountServiceImpl(accountRepository, transactionService, mapper);

    private AccountBo accountBo;

    @Before
    public void setUp() {
        accountBo = TestHelper.getAccountEE1000Bo();
    }

    @Test(expected = AccountNotFoundException.class)
    public void findAccountByNumber_shouldThrowException_whenAccountIsNotFound() {
        // given // when //then
        accountService.findAccountByNumber(TestHelper.ACCOUNT_NUMBER_EE1000);
    }

    @Test
    public void findAccountByNumber_shouldReturnAccount_whenAccountExists() {
        // given
        when(accountRepository.findByAccountNumber(notNull(), notNull())).thenReturn(Optional.of(accountBo));

        // when
        AccountDto foundAccount = accountService.findAccountByNumber(TestHelper.ACCOUNT_NUMBER_EE1000);

        // then
        assertNotNull(foundAccount);
        assertEquals(TestHelper.ACCOUNT_NUMBER_EE1000, foundAccount.getAccountNumber());
        assertEquals(TestHelper.ACCOUNT_BALANCE_1000, foundAccount.getBalance());
    }

    @Test
    public void deposit_shouldIncreaseAccountBalance_whenPositiveAmountIsProvideForDeposit() {
        // given
        BigDecimal amountToAdd = new BigDecimal(900); // initial balance of the account, 'accountBo', is 1000
        when(accountRepository.findByAccountNumber(notNull(), notNull())).thenReturn(Optional.of(accountBo));

        // when
        AccountDto updatedAccount = accountService.deposit(TestHelper.ACCOUNT_NUMBER_EE1000, amountToAdd);

        // then
        assertEquals(new BigDecimal("1900.0"), updatedAccount.getBalance());
    }

    @Test(expected = BalanceInsufficientException.class)
    public void withdraw_shouldThrowBalanceInsufficient_whenAmountProvideForWithdrawalIsMoreThanAvailableBalance() {
        // given
        BigDecimal amountToWithdraw = new BigDecimal(1100); // initial balance of the account, 'accountBo', is 1000
        when(accountRepository.findByAccountNumber(notNull(), notNull())).thenReturn(Optional.of(accountBo));

        // when //then
        AccountDto updatedAccount = accountService.withdraw(TestHelper.ACCOUNT_NUMBER_EE1000, amountToWithdraw);
    }

    @Test
    public void withdraw_shouldDecreaseAccountBalance_whenAmountIsProvideForWithdrawal() {
        // given
        BigDecimal amountToAdd = new BigDecimal(900); // initial balance of the account, 'accountBo', is 1000
        when(accountRepository.findByAccountNumber(notNull(), notNull())).thenReturn(Optional.of(accountBo));

        // when
        AccountDto updatedAccount = accountService.withdraw(TestHelper.ACCOUNT_NUMBER_EE1000, amountToAdd);

        // then
        assertEquals(new BigDecimal("100.0"), updatedAccount.getBalance());
    }

    @Test
    public void prepareAccountStatement_shouldReturnStatementWithEmptyTransactions_whenAccountHasNoTransactions() {
        // given
        when(accountRepository.findByAccountNumber(notNull(), notNull())).thenReturn(Optional.of(accountBo));
        when(transactionService.findTransactionsByAccount(notNull(), notNull())).thenReturn(Page.empty());

        // when
        AccountStatement accountStatement = accountService.prepareAccountStatement(TestHelper.ACCOUNT_NUMBER_EE1000, Pageable.unpaged());

        // then
        assertNotNull(accountStatement);
        assertNotNull(accountStatement.getAccount());
        assertEquals(TestHelper.ACCOUNT_NUMBER_EE1000, accountStatement.getAccount().getAccountNumber());
        assertEquals(new BigDecimal("1000.0"), accountStatement.getAccount().getBalance());

        assertTrue(CollectionUtils.isEmpty(accountStatement.getTransactions().getContent()));
    }

    @Test
    public void prepareAccountStatement_shouldReturnStatementWithTransactions_whenAccountHasNoTransactions() {
        // given
        when(accountRepository.findByAccountNumber(notNull(), notNull())).thenReturn(Optional.of(accountBo));
        Page<TransactionDto> transactions = new PageImpl<>(Lists.newArrayList(TestHelper.getTransaction()), mock(Pageable.class), 1);
        when(transactionService.findTransactionsByAccount(notNull(), notNull())).thenReturn(transactions);

        // when
        AccountStatement accountStatement = accountService.prepareAccountStatement(TestHelper.ACCOUNT_NUMBER_EE1000, Pageable.unpaged());

        // then
        assertNotNull(accountStatement);
        assertNotNull(accountStatement.getAccount());
        assertEquals(TestHelper.ACCOUNT_NUMBER_EE1000, accountStatement.getAccount().getAccountNumber());
        assertEquals(new BigDecimal("1000.0"), accountStatement.getAccount().getBalance());

        List<TransactionDto> retrievedTransactions = accountStatement.getTransactions().getContent();
        assertTrue(CollectionUtils.isNotEmpty(retrievedTransactions));
    }
}
