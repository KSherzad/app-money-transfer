package com.app.money.transfer.accounts.controllers;

import com.app.money.transfer.AbstractControllerIntegrationTest;
import com.app.money.transfer.TestHelper;
import com.app.money.transfer.accounts.models.AccountBo;
import com.app.money.transfer.accounts.repositories.AccountRepository;
import com.app.money.transfer.clients.models.ClientBo;
import com.app.money.transfer.clients.repositories.ClientRepository;
import com.app.money.transfer.exceptions.resources.ErrorType;
import com.app.money.transfer.transactions.models.TransactionBo;
import com.app.money.transfer.transactions.repositories.TransactionRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AccountControllerIntegrationTest extends AbstractControllerIntegrationTest {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    private AccountBo accountEE1000Bo;
    private AccountBo accountEE2000Bo;

    @Before
    public void setUp() {
        super.setUp();
        this.initializeTestDataWithTwoAccounts();
    }

    @Test
    public void retrieveAccountStatement_shouldReturnAccountNotFound_whenAccountProvidedDoesNotExist() throws Exception {
        mockMvc.perform(get("/accounts/someAccountNumber/statement")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorType", is(ErrorType.ACCOUNT_NOT_FOUND_EXCEPTION.name())));
    }

    @Test
    public void retrieveAccountStatement_shouldReturnEmptyTransactionsCollection_whenAccountProvidedHasNoTransactions() throws Exception {
        mockMvc.perform(get("/accounts/" + TestHelper.ACCOUNT_NUMBER_EE1000 + "/statement")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.account", is(notNullValue())))
                .andExpect(jsonPath("$.transactions.content", is(empty())));
    }

    @Test
    public void retrieveTransaction_shouldReturnTransactions_whenAccountProvidedHasTransactions() throws Exception {
        TransactionBo transactionBo = TransactionBo.builder()
                .sourceAccount(this.accountEE1000Bo)
                .destinationAccount(this.accountEE2000Bo)
                .amount(TestHelper.TRANSFER_AMOUNT_100).build();
        transactionRepository.save(transactionBo);

        mockMvc.perform(get("/accounts/" + TestHelper.ACCOUNT_NUMBER_EE1000 + "/statement")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(notNullValue())))
                .andExpect(jsonPath("$.account", is(notNullValue())))
                .andExpect(jsonPath("$.account.accountNumber", is(accountEE1000Bo.getAccountNumber())))
                .andExpect(jsonPath("$.account.balance", is(1000.0)))
                .andExpect(jsonPath("$.transactions", is(notNullValue())))
                .andExpect(jsonPath("$.transactions.content", hasSize(1)))
                .andExpect(jsonPath("$.transactions.content[0].amount", is(100.0)));
    }

    private void initializeTestDataWithTwoAccounts() {
        ClientBo clientBo = this.clientRepository.save(TestHelper.getClientBo());

        accountEE1000Bo = TestHelper.getAccountEE1000Bo();
        accountEE1000Bo.setClient(clientBo);
        this.accountRepository.save(accountEE1000Bo);

        accountEE2000Bo = TestHelper.getAccountEE2000Bo();
        accountEE2000Bo.setClient(clientBo);
        this.accountRepository.saveAndFlush(accountEE2000Bo);
    }
}