package com.app.money.transfer.clients.repositories;

import com.app.money.transfer.clients.models.ClientBo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<ClientBo, Long> {
}
