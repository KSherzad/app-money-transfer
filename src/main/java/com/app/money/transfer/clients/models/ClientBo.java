package com.app.money.transfer.clients.models;

import com.app.money.transfer.accounts.models.AccountBo;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = ClientBo.TABLE_NAME)
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ClientBo  {

    public static final String TABLE_NAME = "client";

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "CLIENT_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "contact", nullable = false)
    private String contact;

    // most probably each client should have chance of multiple accounts, but for simplicity I assume each client has only one account at this point
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "client")
    private List<AccountBo> accounts;
}
