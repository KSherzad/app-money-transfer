package com.app.money.transfer.transfers.controllers;

import com.app.money.transfer.transactions.resources.TransactionDto;
import com.app.money.transfer.transfers.resources.TransferInfo;
import com.app.money.transfer.transfers.services.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;

@RestController
@RequestMapping(TransferController.URL)
@ResponseStatus(HttpStatus.OK)
@Produces("application/json")
@Validated
public class TransferController {

    public static final String URL = "/transfers";
    private final TransferService transferService;

    @Autowired
    public TransferController(final TransferService transferService) {
        this.transferService = transferService;
    }

    @PostMapping
    public TransactionDto executeTransfer(@Validated  @RequestBody TransferInfo transferInfo) {
        return transferService.executeTransfer(transferInfo);
    }
}
