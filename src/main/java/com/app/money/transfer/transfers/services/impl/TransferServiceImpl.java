package com.app.money.transfer.transfers.services.impl;

import com.app.money.transfer.accounts.resources.AccountDto;
import com.app.money.transfer.accounts.services.AccountService;
import com.app.money.transfer.exceptions.TransferInvalidException;
import com.app.money.transfer.transactions.resources.TransactionDto;
import com.app.money.transfer.transactions.services.TransactionService;
import com.app.money.transfer.transfers.resources.TransferInfo;
import com.app.money.transfer.transfers.services.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class TransferServiceImpl implements TransferService {

    private final AccountService accountService;
    private final TransactionService transactionService;

    @Autowired
    public TransferServiceImpl(final AccountService accountService,
                               final TransactionService transactionService){
        this.accountService = accountService;
        this.transactionService = transactionService;
    }


    @Override
    @Transactional
    public TransactionDto executeTransfer(TransferInfo transferInfo) {

        this.throwExceptionIfSourceAndDestinationAreSameAccounts(transferInfo);

        BigDecimal transferAmount = transferInfo.getAmount();
        AccountDto sourceAccount = accountService.withdraw(transferInfo.getSourceAccountNumber(), transferAmount);
        AccountDto destinationAccount = accountService.deposit(transferInfo.getDestinationAccountNumber(), transferAmount);

        TransactionDto transaction = TransactionDto.builder()
                .amount(transferAmount)
                .sourceAccount(sourceAccount)
                .destinationAccount(destinationAccount)
                .build();

        return transactionService.save(transaction);
    }

    private void throwExceptionIfSourceAndDestinationAreSameAccounts(TransferInfo transferInfo) {
        String sourceAccountNumber =  transferInfo.getSourceAccountNumber();
        String destinationAccountNumber =  transferInfo.getDestinationAccountNumber();
        if (sourceAccountNumber.equalsIgnoreCase(destinationAccountNumber)) {
            throw new TransferInvalidException();
        }
    }
}
