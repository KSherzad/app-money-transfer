package com.app.money.transfer.transfers.services;

import com.app.money.transfer.transactions.resources.TransactionDto;
import com.app.money.transfer.transfers.resources.TransferInfo;

public interface TransferService {
    TransactionDto executeTransfer(TransferInfo transferInfo);
}
