package com.app.money.transfer.transfers.resources;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransferInfo {
    @NotBlank(message = "Provide a valid source bank account.")
    private String sourceAccountNumber;

    @NotBlank(message = "Provide a valid destination bank account.")
    private String destinationAccountNumber;

    @NotNull
    @Min(value = 1, message = "The minimum amount should be one or more.")
    private BigDecimal amount;
}
