package com.app.money.transfer.utils;

import org.dozer.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

public class MapperUtil {
    public static <T, U> List<U> mapCollection(final Mapper mapper, final List<T> source, final Class<U> destType) {
        final List<U> dest = new ArrayList<>(source.size());
        for (T element : source) {
            dest.add(mapper.map(element, destType));
        }
        return dest;
    }

    public static <T, U> Page<U> mapCollection(final Mapper mapper, final Page<T> source, final Class<U> destType) {
        final List<U> dest = mapCollection(mapper, source.getContent(), destType);
        return new PageImpl<>(dest, source.getPageable(), source.getTotalElements());
    }
}
