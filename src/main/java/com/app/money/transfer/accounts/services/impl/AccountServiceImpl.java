package com.app.money.transfer.accounts.services.impl;

import com.app.money.transfer.accounts.models.AccountBo;
import com.app.money.transfer.accounts.models.AccountStatement;
import com.app.money.transfer.accounts.repositories.AccountRepository;
import com.app.money.transfer.accounts.resources.AccountDto;
import com.app.money.transfer.accounts.services.AccountService;
import com.app.money.transfer.exceptions.AccountNotFoundException;
import com.app.money.transfer.exceptions.BalanceInsufficientException;
import com.app.money.transfer.transactions.resources.TransactionDto;
import com.app.money.transfer.transactions.services.TransactionService;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final TransactionService transactionService;
    private final Mapper mapper;

    @Autowired
    public AccountServiceImpl(final AccountRepository accountRepository,
                              final TransactionService transactionService,
                              final Mapper mapper) {
        this.accountRepository = accountRepository;
        this.transactionService = transactionService;
        this.mapper = mapper;
    }

    @Override
    @Transactional
    public AccountDto deposit(@NotNull String accountNumber, @NotNull BigDecimal amount) {
        AccountBo accountBo = this.findAccountOrThrowAccountNotFound(accountNumber);
        accountBo.setBalance(accountBo.getBalance().add(amount));
        return mapper.map(accountBo, AccountDto.class);
    }

    @Override
    @Transactional
    public AccountDto withdraw(@NotNull String accountNumber, @NotNull BigDecimal amount) {
        AccountBo accountBo = this.findAccountOrThrowAccountNotFound(accountNumber);
        this.checkAndThrowBalanceInsufficientIfNotEnoughBalance(accountBo, amount);
        accountBo.setBalance(accountBo.getBalance().subtract(amount));
        return mapper.map(accountBo, AccountDto.class);
    }

    private AccountBo findAccountOrThrowAccountNotFound(String accountNumber) {
        return this.accountRepository.findByAccountNumber(accountNumber, LockModeType.PESSIMISTIC_WRITE)
                .orElseThrow(()-> new AccountNotFoundException(accountNumber));
    }

    private void checkAndThrowBalanceInsufficientIfNotEnoughBalance(AccountBo accountBo, BigDecimal amount) {
        if (amount.compareTo(accountBo.getBalance()) > 0) {
            AccountDto account = mapper.map(accountBo, AccountDto.class);
            throw new BalanceInsufficientException(account, amount);
        }
    }

    @Override
    @Transactional
    public AccountDto findAccountByNumber(@NotBlank String accountNumber) {
        AccountBo accountBo = this.findAccountOrThrowAccountNotFound(accountNumber);
        return mapper.map(accountBo, AccountDto.class);
    }

    @Override
    @Transactional
    public AccountStatement prepareAccountStatement(@NotBlank String accountNumber, @NotNull Pageable pageable) {
        AccountDto account = this.findAccountByNumber(accountNumber);
        Page<TransactionDto> transactions = this.transactionService.findTransactionsByAccount(account, pageable);

        AccountStatement accountStatement = new AccountStatement();
        accountStatement.setAccount(account);
        accountStatement.setTransactions(transactions);

        return accountStatement;
    }
}
