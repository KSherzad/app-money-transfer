package com.app.money.transfer.accounts.services;

import com.app.money.transfer.accounts.models.AccountStatement;
import com.app.money.transfer.accounts.resources.AccountDto;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;

public interface AccountService {

    AccountDto withdraw(String sourceAccountNumber, BigDecimal transferAmount);

    AccountDto deposit(String sourceAccountNumber, BigDecimal transferAmount);

    AccountDto findAccountByNumber(String accountNumber);

    AccountStatement prepareAccountStatement(String accountNumber, Pageable pageable);
}
