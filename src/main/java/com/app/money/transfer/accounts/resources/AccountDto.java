package com.app.money.transfer.accounts.resources;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class AccountDto {

    private Long id;

    @NotBlank
    private String accountNumber;

    @NotNull
    private BigDecimal balance;
}
