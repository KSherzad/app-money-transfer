package com.app.money.transfer.accounts.models;

import com.app.money.transfer.accounts.resources.AccountDto;
import com.app.money.transfer.transactions.resources.TransactionDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

@Getter
@Setter
public class AccountStatement {
    private AccountDto account;
    private Page<TransactionDto> transactions;
}
