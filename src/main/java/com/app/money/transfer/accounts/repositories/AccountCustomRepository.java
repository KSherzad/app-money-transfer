package com.app.money.transfer.accounts.repositories;

import com.app.money.transfer.accounts.models.AccountBo;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Optional;

@Repository
public interface AccountCustomRepository {
    Optional<AccountBo> findByAccountNumber(String accountNumber, LockModeType lockType);
}
