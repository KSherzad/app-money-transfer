package com.app.money.transfer.accounts.controllers;

import com.app.money.transfer.accounts.models.AccountStatement;
import com.app.money.transfer.accounts.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;

@RestController
@RequestMapping(AccountController.URL)
@ResponseStatus(HttpStatus.OK)
@Produces("application/json")
public class AccountController {
    public static final String URL = "/accounts";

    private final AccountService accountService;

    @Autowired
    public AccountController(final AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/{accountNumber}/statement")
    public AccountStatement retrieveAccountStatement(@PathVariable String accountNumber, Pageable pageable) {
        AccountStatement accountStatement = accountService.prepareAccountStatement(accountNumber, pageable);
        return accountStatement;
    }
}
