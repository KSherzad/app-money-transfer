package com.app.money.transfer.accounts.repositories;

import com.app.money.transfer.accounts.models.AccountBo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<AccountBo, Long>, AccountCustomRepository {
    Optional<AccountBo> findByAccountNumber(String accountNumber, LockModeType lockType);
}
