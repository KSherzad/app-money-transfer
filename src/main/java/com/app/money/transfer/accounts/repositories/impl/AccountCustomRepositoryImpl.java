package com.app.money.transfer.accounts.repositories.impl;

import com.app.money.transfer.accounts.models.AccountBo;
import com.app.money.transfer.accounts.repositories.AccountCustomRepository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

public class AccountCustomRepositoryImpl implements AccountCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<AccountBo> findByAccountNumber(String accountNumber, LockModeType lockType) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<AccountBo> criteria = builder.createQuery(AccountBo.class);
        Root<AccountBo> from = criteria.from(AccountBo.class);

        criteria.select(from);
        criteria.where(builder.equal(from.get("accountNumber"), accountNumber));
        TypedQuery<AccountBo> typed = entityManager.createQuery(criteria);

        try {
            return Optional.of(typed.setLockMode(lockType).getSingleResult());
        } catch (final NoResultException nre) {
            return Optional.empty();
        }
    }
}
