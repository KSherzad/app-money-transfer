package com.app.money.transfer.transactions.repositories;

import com.app.money.transfer.accounts.models.AccountBo;
import com.app.money.transfer.transactions.models.TransactionBo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionBo, Long> {
    Page<TransactionBo> findAllBySourceAccountOrDestinationAccount(AccountBo sourceAccount,
                                                                   AccountBo destAccount,
                                                                   Pageable pageable);
}
