package com.app.money.transfer.transactions.models;

import com.app.money.transfer.accounts.models.AccountBo;
import com.app.money.transfer.common.models.AbstractAuditBo;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = TransactionBo.TABLE_NAME)
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class TransactionBo extends AbstractAuditBo {

    public static final String TABLE_NAME = "transaction";

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "TRANSACTION_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_sequence")
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "source_account_number", nullable = false, referencedColumnName = "account_number")
    private AccountBo sourceAccount;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "destination_account_number", nullable = false, referencedColumnName = "account_number")
    private AccountBo destinationAccount;

    @Column(name = "amount")
    private BigDecimal amount;
}
