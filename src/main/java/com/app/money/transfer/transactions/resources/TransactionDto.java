package com.app.money.transfer.transactions.resources;

import com.app.money.transfer.accounts.resources.AccountDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionDto {

    private Long id;

    private BigDecimal amount;

    private AccountDto sourceAccount;

    private AccountDto destinationAccount;
}
