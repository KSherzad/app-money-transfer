package com.app.money.transfer.transactions.services;

import com.app.money.transfer.accounts.resources.AccountDto;
import com.app.money.transfer.transactions.resources.TransactionDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TransactionService {
    TransactionDto save(TransactionDto transaction);
    Page<TransactionDto> findTransactionsByAccount(AccountDto account, Pageable pageable);
}
