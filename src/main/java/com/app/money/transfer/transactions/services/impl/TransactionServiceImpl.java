package com.app.money.transfer.transactions.services.impl;

import com.app.money.transfer.accounts.models.AccountBo;
import com.app.money.transfer.accounts.resources.AccountDto;
import com.app.money.transfer.transactions.models.TransactionBo;
import com.app.money.transfer.transactions.repositories.TransactionRepository;
import com.app.money.transfer.transactions.resources.TransactionDto;
import com.app.money.transfer.transactions.services.TransactionService;
import com.app.money.transfer.utils.MapperUtil;
import org.dozer.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final Mapper mapper;

    public TransactionServiceImpl(final TransactionRepository transactionRepository,
                                  final Mapper mapper) {
        this.transactionRepository = transactionRepository;
        this.mapper = mapper;
    }

    @Override
    public TransactionDto save(@NotNull TransactionDto transaction) {
        TransactionBo transactionBo = mapper.map(transaction, TransactionBo.class);
        return mapper.map(transactionRepository.save(transactionBo), TransactionDto.class);
    }

    @Override
    public Page<TransactionDto> findTransactionsByAccount(@NotNull AccountDto account, @NotNull Pageable pageable) {
        AccountBo accountBo = mapper.map(account, AccountBo.class);
        Page<TransactionBo> transactionBos =
                transactionRepository.findAllBySourceAccountOrDestinationAccount(accountBo, accountBo, pageable);
        return MapperUtil.mapCollection(mapper, transactionBos, TransactionDto.class);
    }
}
