package com.app.money.transfer.exceptions;

import com.app.money.transfer.accounts.resources.AccountDto;
import com.app.money.transfer.exceptions.resources.ErrorType;

import java.math.BigDecimal;

public class BalanceInsufficientException extends TransferApplicationRuntimeException {
    private static String MESSAGE_TEMPLATE = "The amount, %f, for withdraw is more than " +
            "the available funds, %f.";

    public BalanceInsufficientException(AccountDto account, BigDecimal withDrawAmount) {
        super(String.format(MESSAGE_TEMPLATE, withDrawAmount, account.getBalance()),
                ErrorType.BALANCE_INSUFFICIENT_EXCEPTION);
    }
}
