package com.app.money.transfer.exceptions;

import com.app.money.transfer.exceptions.resources.ErrorType;
import lombok.Getter;

@Getter
public class TransferApplicationRuntimeException extends RuntimeException{
    private ErrorType errorType;

    public TransferApplicationRuntimeException(String message, ErrorType errorType) {
        super(message);
        this.errorType = errorType;
    }
}
