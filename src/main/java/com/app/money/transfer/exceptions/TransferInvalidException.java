package com.app.money.transfer.exceptions;

import com.app.money.transfer.exceptions.resources.ErrorType;

public class TransferInvalidException extends TransferApplicationRuntimeException {
    private static String MESSAGE_TEMPLATE = "Transfer is not available to exact same account.";

    public TransferInvalidException() {
        super(MESSAGE_TEMPLATE, ErrorType.TRANSFER_INVALID_EXCEPTION);
    }
}
