package com.app.money.transfer.exceptions;

import com.app.money.transfer.exceptions.resources.ErrorType;

public class AccountNotFoundException extends TransferApplicationRuntimeException {
    private static String MESSAGE_TEMPLATE = "Account, %s, does not exist.";

    public AccountNotFoundException(String accountNumber) {
        super(String.format(MESSAGE_TEMPLATE, accountNumber), ErrorType.ACCOUNT_NOT_FOUND_EXCEPTION);
    }
}
