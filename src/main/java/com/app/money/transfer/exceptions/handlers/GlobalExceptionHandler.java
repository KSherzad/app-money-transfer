package com.app.money.transfer.exceptions.handlers;

import com.app.money.transfer.exceptions.TransferApplicationRuntimeException;
import com.app.money.transfer.exceptions.resources.ErrorResponse;
import com.app.money.transfer.exceptions.resources.ErrorType;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse handleApplicationException(final TransferApplicationRuntimeException exception) {
        return new ErrorResponse(exception.getErrorType(), exception.getMessage());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public List<String> handleValidationException(final ConstraintViolationException exception) {
        List<String> validationErrors = exception.getConstraintViolations().stream()
                .map(e -> String.format("%s: %s", getFieldName(e.getPropertyPath()), e.getMessage()))
                .collect(Collectors.toList());
        return validationErrors;
    }

    private String getFieldName(Path path) {
        PathImpl pathImpl = (PathImpl) path;
        return pathImpl.getLeafNode().getName();
    }

    // I'm using javax annotations with Spring frame work validations, then the following exception handler
    // could be made more specific and informative for response. For this assignment with the time constraint
    // I believe it is enough.

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse handleException(final MethodArgumentNotValidException exception) {
        return new ErrorResponse(ErrorType.DATA_INVALID_EXCEPTION, "Provided data is not valid or readable.");
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse handleException(final HttpMessageNotReadableException exception) {
        return new ErrorResponse(ErrorType.DATA_INVALID_EXCEPTION, "Required data is not present or not valid.");
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse handleException(final Exception exception) {
        return new ErrorResponse(ErrorType.INTERNAL_SERVER_ERROR, exception.getMessage());
    }
}
