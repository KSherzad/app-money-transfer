package com.app.money.transfer.exceptions.resources;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

@JsonPropertyOrder({"errorType", "message"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class ErrorResponse {
    String message;
    ErrorType errorType;

    public ErrorResponse(@JsonProperty("errorType") ErrorType errorType,
                         @JsonProperty("message") String message) {
        this.errorType = errorType;
        this.message = message;
    }
}
