CREATE SEQUENCE client_seq
    minvalue 1
    no maxvalue
    start with 1
    increment by 50;

CREATE TABLE client (
    id NUMBER(32) NOT NULL,
    first_name VARCHAR2(100) NOT NULL,
    last_name VARCHAR2(100) NOT NULL,
    address VARCHAR2(150) NOT NULL,
    contact VARCHAR2(100) NOT NULL,
    constraint CLIENT_PK PRIMARY KEY(id)
);