CREATE SEQUENCE transaction_seq
    minvalue 1
    no maxvalue
    start with 1
    increment by 50;

CREATE TABLE transaction(
    id NUMBER(32) NOT NULL,
    source_account_number VARCHAR2(100) NOT NULL,
    destination_account_number VARCHAR2(100) NOT NULL,
    amount NUMBER(32, 2),
    created TIMESTAMP NOT NULL,
    modified TIMESTAMP NOT NULL, -- modified should always equals created
    CONSTRAINT transaction_pk PRIMARY KEY(id),
    CONSTRAINT source_account_fk FOREIGN KEY(source_account_number)
    REFERENCES account(account_number),
    CONSTRAINT destination_account_fk FOREIGN KEY(destination_account_number)
    REFERENCES account(account_number)
);