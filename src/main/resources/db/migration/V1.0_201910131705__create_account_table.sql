CREATE SEQUENCE account_seq
    minvalue 1
    no maxvalue
    start with 1
    increment by 50;

CREATE TABLE account(
    id NUMBER(32) NOT NULL,
    account_number VARCHAR2(100) NOT NULL UNIQUE,
    balance NUMBER(32, 2) NOT NULL,
    client_id NUMBER(32) NOT NULL,
    CONSTRAINT account_pk PRIMARY KEY(id),
    CONSTRAINT account_client_fk FOREIGN KEY(client_id)
    REFERENCES client(id)
);