insert into client(id, first_name, last_name, address, contact)
values(client_seq.nextVal, 'Jan', 'Smith', 'Tallinn', 'Some contact');

insert into account(id, account_number, balance, client_id)
values(account_seq.nextVal, 'EE1', 1000.0, client_seq.currVal);

insert into client(id, first_name, last_name, address, contact)
values(client_seq.nextVal, 'Borre', 'Smith', 'Tallinn', 'Some contact');

insert into account(id, account_number, balance, client_id)
values(account_seq.nextVal, 'EE2', 2000.0, client_seq.currVal);