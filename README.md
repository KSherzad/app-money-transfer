# app-money-transfer

A Java based Spring boot application for money transfer between two predefined accounts.

# How to run:
1. mvn package could be used to package the application.
2. A jar file, i.e. app-money-transfer-1.0-SNAPSHOT.jar, is created in target folder, use 
	java -jar app-money-transfer-1.0-SNAPSHOT.jar to run the application.
3. Applications runs on localhost port 8091.
4. mvn test could be used to execute the tests in the application.

# The application:

This is a simple Java application which satisfy the following requirements;

1. Facilitates sending money between two predefined accounts, with the source having sufficient money for transfer.
2. Facilitates checking account statement that includes account balance and a paged list of transactions.

In order to utilalize the above functionalities the application provides an api which have the following endpoints

1. POST /transfers, consumes a transferInfo object which should have amount, source and destination account numbers.
2. GET /accounts/{accountNumber}/statement, comsumes account number and a pageable object/information.
3. Both API produce a JSON response.

# Testing:

1. A sensible set of Unit and Integration tests are provided for all the functionality in the applicaiton. 
2. One end to end test within the application is provided which uses RestTemplate to call the exposed endpoints. Junit with JsonPath is used to assert the correctness of data. The e2e test asserts the initial balance of two predefined accounts, make a transfer, and validate the final balance. 
3. I have used Apache Jmeter to test the efficiency and consistency of the application with multiple threads/users performing transfers between two same accounts within a time unit of few seconds. For example, a 100 users each had 10 requests in 60 seconds, and the same number of users with the same requests in 10 seconds. In these both cases the error % was zero. During all the tests the state of database was consistent, i.e. account balances with respect to transaction executed.

# Techonology Stack:

1. Java 8.
2. Spring boot.
3. Hibernate.
4. Flyway.
5. Lombok.
6. H2.
7. Junit.
8. RestTemplate for end to end testing.

# Request and Response of the Application:

After application has been run you can perform the following actions, the results will be the same until application status and source code is not updated.

1. Check account EE1 initial balance.

	Request URL:		http://localhost:8091/accounts/EE1/statement
	Method: 		GET
	Response Status: 	200
	Response Body: 	Account Statement Response.

2. Same way account EE2 initial balance could be checked.

3. Check account EE1 initial balance.

	HTTP Request URL:	http://localhost:8091/transfers
	Method: 		POST
	Request Body: 
	{"sourceAccountNumber":"EE2",  "destinationAccountNumber":"EE1", "amount":1}

	Response Status: 	200
	Response Body: 	Transaction Response.